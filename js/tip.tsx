import React from 'react';

const GAP = 20;

const Tip = ({ display, show, data, raw, pos, maxWidth, maxHeight, margin }) => {
  let x = 0;
  let y = 0;
  let width = 100;
  let height = 33;
  let visibility = 'hidden';
  let transform = '';
  let className = '';
  if (display) {
    x = pos.x;
    y = pos.y;
    visibility = 'visible';

    if (x + width / 2 > maxWidth) {
      x = maxWidth - width;
    } else if (x - width / 2 < 0) {
      x = 0;
    } else {
      x = x - width / 2;
    }

    if (y - height - GAP / 2 > 0) {
      y = y - height - GAP / 2;
    } else {
      y = y + GAP / 2;
    }

    transform = `translate(${x},${y})`;

    if (!show && maxWidth / raw.length < GAP * 5) {
      className = 'transition';
    }
  }

  return (
    <g visibility={visibility} transform={transform} id='line-chart-tip' className={className}>
      <rect class='shadow' is width={width} height={height} rx='2' ry='2' visibility={visibility} />
      <text is visibility={visibility} transform={`translate(${width / 2},0)`}>
        <tspan is x='0' text-anchor='middle' dy='15' font-size='14px' font-weight='bold'>{data.date}</tspan>
        <tspan is x='0' text-anchor='middle' dy='14' font-size='12px'>{data.value}</tspan>
      </text>
    </g>
  );
};

export default Tip;

