import React from 'react';
import { area } from 'd3-shape';

export default class Area extends React.Component {
  render () {
    var { data, target, x, y, width, height, margin } = this.props;
    if (data.length === 1) {
      return <g />;
    }

    var belowLine = area()
      .x(d => x(d.date))
      .y0(height)
      .y1(d => y(d[target]));

    var d = belowLine(data);

    return (
      <g>
        <path className='area below-line' d={d} />
        <rect className='area below-line'
          x={-margin.left} y={y(data[data.length - 1][target])}
          width={margin.left - 1} height={height - y(data[data.length - 1][target])} />
        <rect className='area below-line'
          x={width - margin.left - margin.right + 1} y={y(data[0][target])}
          width={margin.right - 1} height={height - y(data[0][target])} />
      </g>
    );
  }
}
