import React from 'react';
import Measure from 'react-measure';
import Area from './area';
import Axis from './axis';
import Tick from './tick';
import Dots from './dots';
import Tip from './tip';

import { scaleTime, scaleLinear } from 'd3-scale';
import { axisLeft, axisBottom } from 'd3-axis';
import { timeFormat } from 'd3-time-format';
import { format } from 'd3-format';
import { line } from 'd3-shape';
import { bisector } from 'd3-array';
import xDomain from './x-domain';
import yDomain from './y-domain';

const OUTER_MARGIN = 20;
const X_AXIS_SPACE = 30;
const MARGIN = 4;

var dateFormat = timeFormat('%m/%d');
var valueFormat = format(',');

var bisectDate = bisector((a, b) => a.date.getTime() - b.date.getTime()).left;

class LineChart extends React.Component {
  state = {
    dimensions: {
      bounds: {
        width: this.props.width,
        height: this.props.height
      }
    },
    highlightIndex: null,
    tip: {
      display: false,
      data: { date: '', value: '' },
      pos: { x: 0, y: 0 }
    }
  }

  hideTip = event => {
    this.setState({
      highlightIndex: null,
      tip: {
        display: false,
        data: { date: '', value: '' },
        pos: { x: 0, y: 0 }
      }
    });
  }

  onMouseMove = (x, y) => {
    return (event) => {
      var data = this.props.data.slice();
      data.reverse();
      var x0 = x.invert(event.nativeEvent.offsetX - OUTER_MARGIN - MARGIN);
      var i = bisectDate(data, {date: x0});
      if (i === 0) { i = 1; }
      if (i === data.length) { i = data.length - 1; }
      var d1 = data[i];
      var d0 = data[i - 1];
      var [hi, d] = [i, d1];
      if (data.length > 1) {
        [hi, d] = x0 - d0.date < d1.date - x0 ? [i - 1, d0] : [i, d1];
      }
      this.setState({
        highlightIndex: data.length - hi - 1,
        tip: {
          display: true,
          show: !this.state.tip.display,
          data: {
            date: dateFormat(d.date),
            value: valueFormat(d[this.props.target])
          },
          pos: {
            x: x(d.date),
            y: y(d[this.props.target])
          }
        }
      });
    };
  }

  render () {
    const { data, target } = this.props;

    const outerMargin = {left: OUTER_MARGIN, right: OUTER_MARGIN};
    const margin = {top: 10, right: MARGIN, bottom: X_AXIS_SPACE, left: MARGIN};
    const graphWidth = this.state.dimensions.bounds.width - outerMargin.left - outerMargin.right; // x-axis line width
    const pointWidth = graphWidth - margin.left - margin.right; // point center, first to last
    const height = this.props.height - margin.top - margin.bottom;

    // Set the ranges
    const x = scaleTime().range([0, pointWidth]);
    const y = scaleLinear().range([height, 0]);

    // Define the axes
    const xTicks = data.length < 5 ? data.length < 2 ? 1 : data.length - 1 : 5;
    const xAxis = axisBottom(x).ticks(xTicks)
      .tickSizeInner(0)
      .tickSizeOuter(0)
      .tickPadding(10)
      .tickFormat(dateFormat);

    const yAxis = axisLeft(y)
      .tickSizeInner(-pointWidth - margin.left - margin.right)
      .tickSizeOuter(0)
      .tickPadding(0);

    const xBoundary = xDomain(data);
    const yBoundary = yDomain(data, target);

    yAxis.tickValues([yBoundary[0], yBoundary[1] / 2, yBoundary[1]]);

    // Scale the range of the data
    x.domain(xBoundary);
    y.domain(yBoundary);

    // Define the line
    const valueline = line()
      .x(function (d) { return x(d.date); })
      .y(function (d) { return y(d[target]); });

    const transform = `translate(${outerMargin.left + margin.left},${margin.top})`;
    const captureTransform = `translate(${outerMargin.left + margin.left},${margin.top})`;

    return (
      <Measure bounds
        onResize={(dimensions) => {
          this.setState({dimensions});
        }}
      >
        {({ measure, measureRef, contentRect }) => (
          <div id='major-graph' ref={measureRef}>
            <svg width={this.state.dimensions.bounds.width} height={this.props.height} className='line-chart'>
              <g transform={transform} >
                <Axis h={height} axis={xAxis} axisType='x' />
                <Axis h={height} axis={yAxis} axisType='y' margin={margin} />
                <Area data={data} target={target} x={x} y={y} width={graphWidth} height={height} margin={margin} />
                <path d={valueline(data)} className='value-line' />
                <Dots data={data} target={target} x={x} y={y} highlightIndex={this.state.highlightIndex} />
                <Tick h={height} axis={yAxis} axisType='y' margin={margin} />
                <Tip raw={data} maxWidth={pointWidth} maxHeight={height} margin={margin} {...this.state.tip} />
              </g>
              <rect transform={captureTransform}
                width={this.state.dimensions.bounds.width - margin.left - margin.right} height={height}
                onMouseOut={this.hideTip}
                onMouseMove={this.onMouseMove(x, y, this)}
                fill='none' pointerEvents='all' />
            </svg>
          </div>
        )}
      </Measure>
    );
  }
}

export default LineChart;
