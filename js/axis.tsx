import React from 'react';
import { findDOMNode } from 'react-dom';

import { select } from 'd3-selection';

export default class Axis extends React.Component {
  componentDidUpdate () { this.renderAxis(); }

  componentDidMount () { this.renderAxis(); }

  renderAxis () {
    var node = findDOMNode(this);
    select(node).call(this.props.axis);
    if (this.props.axisType === 'y') {
      select(node).selectAll('.tick line').attr('transform', `translate(${-this.props.margin.left},0)`);
      select(node).selectAll('text').remove();
    }
  }

  render () {
    var translate = `translate(0,${this.props.h})`;

    return (
      <g className={`axis ${this.props.axisType}`} transform={this.props.axisType === 'x' ? translate : ''} />
    );
  }
}

