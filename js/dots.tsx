import React from 'react';

const R = 4;

class Dots extends React.Component {
  render () {
    var { data, target, x, y, highlightIndex } = this.props;

    var defaultR = R;

    if (data.length > 30) { defaultR--; }
    if (data.length > 45) { defaultR--; }
    if (data.length > 60) { defaultR--; }

    var r = (i) => {
      if (i === highlightIndex) {
        return R + 2;
      }
      if (data.length === 1) {
        return R + 2;
      }
      if (i === 0 || i === data.length - 1) {
        return R;
      }
      return defaultR;
    };

    var circles = data.map((d, i) => (
      <circle r={r(i)} cx={x(d.date)} cy={y(d[target])} key={i} />
    ));

    return (
      <g>{circles}</g>
    );
  }
}

export default Dots;
