import React from 'react';
import { findDOMNode } from 'react-dom';

import { select } from 'd3-selection';

export default class Axis extends React.Component {
  componentDidUpdate () { this.renderAxis(); }

  componentDidMount () { this.renderAxis(); }

  renderAxis () {
    var node = findDOMNode(this);
    select(node).call(this.props.axis);
    select(node).selectAll('.tick line').remove();
    if (this.props.axisType === 'y') {
      let labels = select(node).selectAll('text');
      let labelNodes = labels.nodes();
      let labelOffset = labelNodes[labelNodes.length - 1].getBBox();
      labels.attr('transform', `translate(${labelOffset.width},${labelOffset.height - 4})`);
    }
  }

  render () {
    var translate = `translate(0,${this.props.h})`;

    return (
      <g className={`axis ${this.props.axisType}`} transform={this.props.axisType === 'x' ? translate : ''} />
    );
  }
}

