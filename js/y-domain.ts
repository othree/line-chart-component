import { max } from 'd3-array';

var log10 = function (x) {
  return Math.log(x) * Math.LOG10E;
};

const MAPPING = [
  // [first digit, log offset]
  [0, 0], // 0
  [2, 0], // 1
  [4, 0], // 2
  [4, 0], // 3
  [8, 0], // 4
  [8, 0], // 5
  [8, 0], // 6
  [1, 1], // 7
  [2, 1], // 8
  [2, 1]  // 9
];

export default function (data, target) {
  var maxValue = max(data, function (d) { return d[target]; });
  var firstDigit = parseInt(maxValue.toString()[0], 10);
  var digits = maxValue ? Math.floor(log10(maxValue)) : 0;
  var ytop = MAPPING[firstDigit];

  if (digits === 0 && firstDigit === 0) {
    return [0, 100];
  }

  return [0, ytop[0] * Math.pow(10, digits + ytop[1])];
}
