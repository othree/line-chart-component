import { extent } from 'd3-array';

const H12 = 60 * 60 * 12 * 1000;

export default function (data) {
  if (data.length === 1) {
    let timestamp = data[0].date.getTime();
    return [new Date(timestamp - H12), new Date(timestamp + H12)];
  } else {
    return extent(data, function (d) { return d.date; });
  }
}
